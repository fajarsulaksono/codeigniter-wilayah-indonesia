        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Rekap Wilayah (Dati 1/Dati 2/Dati 3/Dati 4)</h1>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
                                <li class="breadcrumb-item active">Rekap Wilayah</li>
                            </ol>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="clear_20"></div>

<?php if (!empty($search_by)) { ?>
<a class="btn btn-warning" href="<?php echo site_url($base_path . '/refresh'); ?>"><i class="fas fa-sync"></i> Clear Search Result</a>
<?php } else { ?>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#searchModal">
    <i class="fas fa-search"></i> Search Data
    </button>
<?php } ?>

<a class="btn btn-success" href="<?php echo site_url($base_path . '/export/xlsx');?>"><i class="fa fa-file-excel"></i> Export Excel</a>
<a class="btn btn-success" href="<?php echo site_url($base_path . '/export/csv');?>"><i class="fa fa-file-csv"></i> Export CSV</a>
    <div class="clear_20"></div>

<?php
	if (count($rows) > 0) {
        echo $pagination;
        $i = $page_offset + 1;

        foreach ($rows as $row) {
            $this->table->add_row(
                $row['id'],
                $row['dati_1'],
                $row['dati_2'],
                $row['dati_3'],
                $row['dati_4'],
                $row['kode_pos'],
            );

            $i++;
        } //end foreach
        echo '<div class="clear_0"></div><b>Total Data : '.$total_rows.'</b><div class="clear_0"></div>';
        echo $this->table->generate();
        echo $pagination;
	} else {
	    echo 'Data tidak ditemukan';
	}
?>
        </div>

        <div class="clear_80"></div>
        </div>
    </div>
    <!-- /.row (main row) -->
</div>
<!-- /.container-fluid -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Modal -->
<div class="modal fade" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <?php
    $attributes = array('class' => 'modalFormSearch', 'id' => 'modalFormSearch');
    echo form_open(site_url($base_path . '/search_by'), $attributes);
    ?>
      <div class="modal-header">
        <h5 class="modal-title" id="searchModalLabel">Search Data Rekap Wilayah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

            <label for="kode">Kode</label>
            <input class="form-control" type="text" name="kode" id="kode"/>
            <label for="dati_1">Dati 1</label>
            <input class="form-control" type="text" name="dati_1" id="dati_1"/>

            <label for="dati_2">Dati 2</label>
            <input class="form-control" type="text" name="dati_2" id="dati_2"/>

            <label for="dati_3">Dati 3</label>
            <input class="form-control" type="text" name="dati_3" id="dati_3"/>

            <label for="dati_4">Dati 4</label>
            <input class="form-control" type="text" name="dati_4" id="dati_4"/>

            <label for="nama">Kode Pos</label>
            <input class="form-control" type="text" name="kode_pos" id="kode_pos"/>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Submit Search</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dati_2_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }//end __construct

    function getDati2($per_page, $page_offset, $search_by = [], $ACT = false)
    {
        if (($ACT == 'PARTIAL') || ($ACT == 'LAST_QUERY')) {
            $this->db->limit($per_page, $page_offset);
        }

        if (!empty($search_by)){
            if ($search_by['id'] != '') {
                $this->db->where('t1.id', $search_by['id']);
            }

            if ($search_by['nama'] != '') {
                $this->db->like('t1.nama', $search_by['nama']);
            }

            if ($search_by['type'] != '') {
                $this->db->where('t1.type', $search_by['type']);
            }
        }

        $this->db->order_by('t1.id', 'ASC');
        $this->db->from('dati_2 t1');

        if ($ACT == 'NUM_ROWS') {
            $this->db->select('t1.id');
        } else {
            $this->db->select('t1.*');
        }

        $query = $this->db->get();
        $last_query = $this->db->last_query();

        if ($query->num_rows() > 0) {
            if ($ACT == 'PARTIAL') {
                foreach ($query->result_array() as $row) {
                    $data[] = $row;
                }

                return $data;
            } elseif ($ACT == 'EXCEL') {
                return $last_query;
            } elseif ($ACT == 'NUM_ROWS') {
                $RES = $query->num_rows();
                return $RES;
            }
        }
    }//end getDati2()

}
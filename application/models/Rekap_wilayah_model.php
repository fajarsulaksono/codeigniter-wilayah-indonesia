<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_wilayah_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }//end __construct

    function getRekapWilayah($per_page, $page_offset, $search_by = [], $ACT = false)
    {
        if (($ACT == 'PARTIAL') || ($ACT == 'LAST_QUERY')) {
            $this->db->limit($per_page, $page_offset);
        }

        if (!empty($search_by)){
            if ($search_by['id'] != '') {
                $this->db->where('t1.id', $search_by['id']);
            }

            if ($search_by['dati_1'] != '') {
                $this->db->like('t2.nama', $search_by['dati_1']);
            }

            if ($search_by['dati_2'] != '') {
                $this->db->like('t4.nama', $search_by['dati_2']);
            }

            if ($search_by['dati_3'] != '') {
                $this->db->like('t3.nama', $search_by['dati_3']);
            }

            if ($search_by['dati_4'] != '') {
                $this->db->like('t1.nama', $search_by['dati_4']);
            }

            if ($search_by['kode_pos'] != '') {
                $this->db->like('t1.zip', $search_by['kode_pos']);
            }
        }
        /*
        SELECT t1.id,t2.nama AS dati_1,t4.nama AS dati_2,t3.nama AS dati_3,t1.nama AS dati_4,t1.zip AS kode_pos
        FROM dati_4 t1
        LEFT JOIN dati_2 t4 ON LEFT(t1.id, 5) = t4.id
        LEFT JOIN dati_3 t3 ON LEFT(t1.id, 8) = t3.id
        LEFT JOIN dati_1 t2 ON LEFT(t1.id, 2) = t2.id
        */
        $this->db->order_by('t1.id', 'ASC');
        $this->db->join('dati_2 t4', 'LEFT(t1.id, 5) = t4.id', 'left');
        $this->db->join('dati_3 t3', 'LEFT(t1.id, 8) = t3.id', 'left');
        $this->db->join('dati_1 t2', 'LEFT(t1.id, 2) = t2.id', 'left');
        $this->db->from('dati_4 t1');

        if ($ACT == 'NUM_ROWS') {
            $this->db->select('t1.id');
        } else {
            $this->db->select('t1.id,t2.nama AS dati_1,t4.nama AS dati_2,t3.nama AS dati_3,t1.nama AS dati_4,t1.zip AS kode_pos');
        }

        $query = $this->db->get();
        $last_query = $this->db->last_query();

        if ($query->num_rows() > 0) {
            if ($ACT == 'PARTIAL') {
                foreach ($query->result_array() as $row) {
                    $data[] = $row;
                }

                return $data;
            } elseif ($ACT == 'EXCEL') {
                return $last_query;
            } elseif ($ACT == 'NUM_ROWS') {
                $RES = $query->num_rows();
                return $RES;
            }
        }
    }//end getDati1()

}
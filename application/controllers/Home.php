<?php defined('BASEPATH') OR exit('No direct script access allowed');
//load Spout Library
require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('table', 'pagination'));
        $this->load->helper(array('form'));
        $this->param = $this->get_uri_array();
        $this->main_title = 'Home';

        $this->load->model('rekap_wilayah_model', 'rekap_wilayah_model');
        $this->load->model('dati_1_model', 'dati_1_model');
        $this->load->model('dati_2_model', 'dati_2_model');
        $this->load->model('dati_3_model', 'dati_3_model');
        $this->load->model('dati_4_model', 'dati_4_model');

        $this->post = [];
        foreach ($this->input->post() as $key => $value) {
            $this->post[$key] = $value;
        }//

    }

	public function index()
	{
        $data = [
            'title' => 'Home'
        ];

		$this->_render_page('home/index', 'master_home', $data);
	}

    public function rekap_wilayah()
    {
        $data['func'] = __FUNCTION__;
        $param = $this->param;
        isset($param[3]) ? '' : $param[3] = '';
        isset($param[4]) ? '' : $param[4] = '';
        $displayed = array();
        $data['sess_segment'] = $this->session->userdata('sess_segment');
        $data['base_path'] = strtolower(__CLASS__).'/'.__FUNCTION__;
        $post = $this->post;

        switch ($param[3]) {
            case 'export':
                $date_today = $today = date("Y_m_d_H_i_s");

                if ($param[4] == 'csv') {
                    $writer = WriterEntityFactory::createCSVWriter();
                    //stream to browser
                    $writer->openToBrowser("export_rekap_wilayah_".$date_today.".csv");
                } else {
                    $writer = WriterEntityFactory::createXLSXWriter();
                    //stream to browser
                    $writer->openToBrowser("export_rekap_wilayah_".$date_today.".xlsx");
                }

                $header = [
                    WriterEntityFactory::createCell('ID'),
                    WriterEntityFactory::createCell('Dati 1'),
					WriterEntityFactory::createCell('Kode Provinsi'),
                    WriterEntityFactory::createCell('Dati 2'),
					WriterEntityFactory::createCell('Kode Kabupaten/Kota'),
                    WriterEntityFactory::createCell('Dati 3'),
                    WriterEntityFactory::createCell('Dati 4'),
                    WriterEntityFactory::createCell('Kode Pos'),
                ];

                /** Tambah row satu kali untuk header */
                $singleRow = WriterEntityFactory::createRow($header);
                $writer->addRow($singleRow); //tambah row untuk header data

                $excel    = $this->session->userdata(__FUNCTION__.'_query');
                $SQL_get  = $excel;
                $RES_get  = $this->db->query($SQL_get);

                $i = 1;
                $data = array();

                foreach ( $RES_get->result_array() as $row ) {
                    $arr = [
                        WriterEntityFactory::createCell($row['id']),
                        WriterEntityFactory::createCell($row['dati_1']),
						WriterEntityFactory::createCell($row['kode_provinsi']),
                        WriterEntityFactory::createCell($row['dati_2']),
						WriterEntityFactory::createCell($row['kode_kabupaten_kota']),
                        WriterEntityFactory::createCell($row['dati_3']),
                        WriterEntityFactory::createCell($row['dati_4']),
                        WriterEntityFactory::createCell($row['kode_pos']),
                    ];

                    array_push($data, WriterEntityFactory::createRow($arr));
                }//end foreach

                $writer->addRows($data); // add multiple rows at a time
                $writer->close();
                break;

            case 'refresh':
                $this->session->unset_userdata(__FUNCTION__ . '_search_by');
                $this->session->unset_userdata(__FUNCTION__ . '_query');

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            case 'search_by':
                if (isset($post['id'])){ $search_by['id']      = $post['id']; }else{$search_by['id'] = ''; }
                if (isset($post['dati_1'])){ $search_by['dati_1']      = $post['dati_1']; }else{$search_by['dati_1'] = ''; }
                if (isset($post['dati_2'])){ $search_by['dati_2']      = $post['dati_2']; }else{$search_by['dati_2'] = ''; }
                if (isset($post['dati_3'])){ $search_by['dati_3']      = $post['dati_3']; }else{$search_by['dati_3'] = ''; }
                if (isset($post['dati_4'])){ $search_by['dati_4']      = $post['dati_4']; }else{$search_by['dati_4'] = ''; }
                if (isset($post['kode_pos'])){ $search_by['kode_pos']      = $post['kode_pos']; }else{$search_by['kode_pos'] = ''; }

                $this->session->set_userdata( __FUNCTION__.'_search_by', $search_by );

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            default:
                $search_by = $this->session->userdata( __FUNCTION__.'_search_by');

                $this->table->set_template($this->tmpl);
                $this->table->set_heading('ID', 'Dati 1', 'Kode Provinsi', 'Dati 2', 'Kode Kabupaten/Kota', 'Dati 3', 'Dati 4', 'Kode Pos');
                $data['page_offset']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $config_pagination['per_page']  = '30';
                $config_pagination              = array_merge($this->config_pagination, $config_pagination);
                $config_pagination['base_url']  = site_url(strtolower(__CLASS__).'/'.__FUNCTION__);

                $data['total_rows']             = $config_pagination['total_rows'] = $this->rekap_wilayah_model->getRekapWilayah(NULL, NULL, $search_by, 'NUM_ROWS' );

                $this->pagination->initialize($config_pagination);

                $data['pagination']     = $this->pagination->create_links();
                $data['rows']      = $this->rekap_wilayah_model->getRekapWilayah($config_pagination['per_page'], $data['page_offset'], $search_by, 'PARTIAL' );
                $data['jml_segmen'] = $this->session->userdata('jml_segmen_article');
                $excel = $this->rekap_wilayah_model->getRekapWilayah($config_pagination['per_page'], $data['page_offset'], $search_by, 'EXCEL' );
                $this->session->set_userdata(__FUNCTION__.'_query', $excel);
                $this->session->set_userdata( 'sess_segment_'.__FUNCTION__, $param[3] );
                ( null !== $this->session->flashdata('result') ) ? $data['result'] = $this->session->flashdata('result') : $data['result'] = '';
                $data['search_by'] = $search_by;

                $content = $this->load->view('home/'.__FUNCTION__, $data, TRUE);

                $displayed = array(
                    'base_path' => $config_pagination['base_url'],
                    'main_title' =>  $this->main_title,
                    'title' => 'Rekap Wilayah',
                    'content' => $content
                );

                $this->parser->parse('_template/master_home', $displayed);
                break;
        }//end switch

    }

    public function dati_1()
    {
        $data['func'] = __FUNCTION__;
        $param = $this->param;
        isset($param[3]) ? '' : $param[3] = '';
        isset($param[4]) ? '' : $param[4] = '';
        $displayed = array();
        $data['sess_segment'] = $this->session->userdata('sess_segment');
        $data['base_path'] = strtolower(__CLASS__).'/'.__FUNCTION__;
        $post = $this->post;

        switch ($param[3]) {
            case 'export':
                $date_today = $today = date("Y_m_d_H_i_s");

                if ($param[4] == 'csv') {
                    $writer = WriterEntityFactory::createCSVWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_1_".$date_today.".csv");
                } else {
                    $writer = WriterEntityFactory::createXLSXWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_1_".$date_today.".xlsx");
                }

                $header = [
                    WriterEntityFactory::createCell('ID'),
                    WriterEntityFactory::createCell('Nama'),
					WriterEntityFactory::createCell('Kode Provinsi'),
                ];

                /** Tambah row satu kali untuk header */
                $singleRow = WriterEntityFactory::createRow($header);
                $writer->addRow($singleRow); //tambah row untuk header data

                $excel    = $this->session->userdata(__FUNCTION__.'_query');
                $SQL_get  = $excel;
                $RES_get  = $this->db->query($SQL_get);

                $i = 1;
                $data = array();

                foreach ( $RES_get->result_array() as $row ) {
                    $arr = [
                        WriterEntityFactory::createCell($row['id']),
                        WriterEntityFactory::createCell($row['nama']),
						WriterEntityFactory::createCell($row['kode_provinsi']),
                    ];

                    array_push($data, WriterEntityFactory::createRow($arr));
                }//end foreach

                $writer->addRows($data); // add multiple rows at a time
                $writer->close();
                break;

            case 'refresh':
                $this->session->unset_userdata(__FUNCTION__ . '_search_by');
                $this->session->unset_userdata(__FUNCTION__ . '_query');

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            case 'search_by':
                //var_dump($post);exit();
                if (isset($post['id'])){ $search_by['id']      = $post['id']; }else{$search_by['id'] = ''; }
                if (isset($post['nama'])){ $search_by['nama']      = $post['nama']; }else{$search_by['nama'] = ''; }

                $this->session->set_userdata( __FUNCTION__.'_search_by', $search_by );

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            default:
                $search_by = $this->session->userdata( __FUNCTION__.'_search_by');

                $this->table->set_template($this->tmpl);
                $this->table->set_heading('ID', 'Nama', 'Kode Provinsi');
                $data['page_offset']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $config_pagination['per_page']  = '30';
                $config_pagination              = array_merge($this->config_pagination, $config_pagination);
                $config_pagination['base_url']  = site_url($data['base_path']);

                $data['total_rows']             = $config_pagination['total_rows'] = $this->dati_1_model->getDati1(NULL, NULL, $search_by, 'NUM_ROWS' );

                $this->pagination->initialize($config_pagination);

                $data['pagination']     = $this->pagination->create_links();
                $data['rows']      = $this->dati_1_model->getDati1($config_pagination['per_page'], $data['page_offset'], $search_by, 'PARTIAL' );
                $data['jml_segmen'] = $this->session->userdata('jml_segmen_article');
                $excel = $this->dati_1_model->getDati1($config_pagination['per_page'], $data['page_offset'], $search_by, 'EXCEL' );
                $this->session->set_userdata(__FUNCTION__.'_query', $excel);
                $this->session->set_userdata( 'sess_segment_'.__FUNCTION__, $param[3] );
                ( null !== $this->session->flashdata('result') ) ? $data['result'] = $this->session->flashdata('result') : $data['result'] = '';
                $data['search_by'] = $search_by;

                $content = $this->load->view('home/'.__FUNCTION__, $data, TRUE);

                $displayed = array(
                    'base_path' => $config_pagination['base_url'],
                    'main_title' =>  $this->main_title,
                    'title' => 'Dati 1',
                    'content' => $content
                );

                $this->parser->parse('_template/master_home', $displayed);
                break;
        }//end switch

    }//end dati_1

    public function dati_2()
    {
        $data['func'] = __FUNCTION__;
        $param = $this->param;
        isset($param[3]) ? '' : $param[3] = '';
        isset($param[4]) ? '' : $param[4] = '';

        $displayed = array();
        $data['sess_segment'] = $this->session->userdata('sess_segment');
        $data['base_path'] = strtolower(__CLASS__).'/'.__FUNCTION__;
        $post = $this->post;

        switch ($param[3]) {
            case 'export':
                $date_today = $today = date("Y_m_d_H_i_s");

                if ($param[4] == 'csv') {
                    $writer = WriterEntityFactory::createCSVWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_2_".$date_today.".csv");
                } else {
                    $writer = WriterEntityFactory::createXLSXWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_2_".$date_today.".xlsx");
                }

                $header = [
                    WriterEntityFactory::createCell('ID'),
                    WriterEntityFactory::createCell('Nama'),
                    WriterEntityFactory::createCell('Tipe'),
					WriterEntityFactory::createCell('Kode Kabupaten Kota'),
                    WriterEntityFactory::createCell('Kode Provinsi'),
                ];

                /** Tambah row satu kali untuk header */
                $singleRow = WriterEntityFactory::createRow($header);
                $writer->addRow($singleRow); //tambah row untuk header data

                $excel    = $this->session->userdata(__FUNCTION__.'_query');
                $SQL_get  = $excel;
                $RES_get  = $this->db->query($SQL_get);

                $i = 1;
                $data = array();

                foreach ( $RES_get->result_array() as $row ) {
                    $arr = [
                        WriterEntityFactory::createCell($row['id']),
                        WriterEntityFactory::createCell($row['nama']),
                        WriterEntityFactory::createCell($row['type']),
						WriterEntityFactory::createCell($row['kode_kabupaten_kota']),
                        WriterEntityFactory::createCell($row['kode_provinsi']),
                    ];

                    array_push($data, WriterEntityFactory::createRow($arr));
                }//end foreach

                $writer->addRows($data); // add multiple rows at a time
                $writer->close();
                break;

            case 'refresh':
                $this->session->unset_userdata(__FUNCTION__ . '_search_by');
                $this->session->unset_userdata(__FUNCTION__ . '_query');

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            case 'search_by':
                if (isset($post['id'])){ $search_by['id']      = $post['id']; }else{$search_by['id'] = ''; }
                if (isset($post['nama'])){ $search_by['nama']      = $post['nama']; }else{$search_by['nama'] = ''; }
                if (isset($post['type'])){ $search_by['type']      = $post['type']; }else{$search_by['type'] = ''; }

                $this->session->set_userdata( __FUNCTION__.'_search_by', $search_by );

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            default:
                $search_by = $this->session->userdata( __FUNCTION__.'_search_by');

                $this->table->set_template($this->tmpl);
                $this->table->set_heading('ID', 'Nama', 'Tipe', 'Kode Kabupaten/Kota', 'Kode Provinsi');
                $data['page_offset']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $config_pagination['per_page']  = '30';
                $config_pagination              = array_merge($this->config_pagination, $config_pagination);
                $config_pagination['base_url']  = site_url($data['base_path']);

                $data['total_rows']             = $config_pagination['total_rows'] = $this->dati_2_model->getDati2(NULL, NULL, $search_by, 'NUM_ROWS' );

                $this->pagination->initialize($config_pagination);

                $data['pagination']     = $this->pagination->create_links();
                $data['rows']      = $this->dati_2_model->getDati2($config_pagination['per_page'], $data['page_offset'], $search_by, 'PARTIAL' );
                $data['jml_segmen'] = $this->session->userdata('jml_segmen_article');
                $excel = $this->dati_2_model->getDati2($config_pagination['per_page'], $data['page_offset'], $search_by, 'EXCEL' );

                $this->session->set_userdata(__FUNCTION__.'_query', $excel);
                $this->session->set_userdata( 'sess_segment_'.__FUNCTION__, $param[3] );
                ( null !== $this->session->flashdata('result') ) ? $data['result'] = $this->session->flashdata('result') : $data['result'] = '';
                $data['search_by'] = $search_by;

                $content = $this->load->view('home/'.__FUNCTION__, $data, TRUE);

                $displayed = array(
                    'main_title' =>  $this->main_title,
                    'title' => 'Dati 2',
                    'content' => $content
                );

                $this->parser->parse('_template/master_home', $displayed);
                break;
        }//end switch
    }

    public function dati_3()
    {
        $data['func'] = __FUNCTION__;
        $param = $this->param;
        isset($param[3]) ? '' : $param[3] = '';
        isset($param[4]) ? '' : $param[4] = '';
        $displayed = array();
        $data['sess_segment'] = $this->session->userdata('sess_segment');
        $data['base_path'] = strtolower(__CLASS__).'/'.__FUNCTION__;
        $post = $this->post;

        switch ($param[3]) {
           case 'export':
                $date_today = $today = date("Y_m_d_H_i_s");

                if ($param[4] == 'csv') {
                    $writer = WriterEntityFactory::createCSVWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_3_".$date_today.".csv");
                } else {
                    $writer = WriterEntityFactory::createXLSXWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_3_".$date_today.".xlsx");
                }

                $header = [
                    WriterEntityFactory::createCell('ID'),
                    WriterEntityFactory::createCell('Nama'),
                ];

                /** Tambah row satu kali untuk header */
                $singleRow = WriterEntityFactory::createRow($header);
                $writer->addRow($singleRow); //tambah row untuk header data

                $excel    = $this->session->userdata(__FUNCTION__.'_query');
                $SQL_get  = $excel;
                $RES_get  = $this->db->query($SQL_get);

                $i = 1;
                $data = array();

                foreach ( $RES_get->result_array() as $row ) {
                    $arr = [
                        WriterEntityFactory::createCell($row['id']),
                        WriterEntityFactory::createCell($row['nama']),
                    ];

                    array_push($data, WriterEntityFactory::createRow($arr));
                }//end foreach

                $writer->addRows($data); // add multiple rows at a time
                $writer->close();
                break;

            case 'refresh':
                $this->session->unset_userdata(__FUNCTION__ . '_search_by');
                $this->session->unset_userdata(__FUNCTION__ . '_query');

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            case 'search_by':
                if (isset($post['id'])){ $search_by['id']      = $post['id']; }else{$search_by['id'] = ''; }
                if (isset($post['nama'])){ $search_by['nama']      = $post['nama']; }else{$search_by['nama'] = ''; }

                $this->session->set_userdata( __FUNCTION__.'_search_by', $search_by );

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            default:
                $search_by = $this->session->userdata( __FUNCTION__.'_search_by');

                $this->table->set_template($this->tmpl);
                $this->table->set_heading('ID', 'Nama');
                $data['page_offset']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $config_pagination['per_page']  = '30';
                $config_pagination              = array_merge($this->config_pagination, $config_pagination);
                $config_pagination['base_url']  = site_url($data['base_path']);

                $data['total_rows']             = $config_pagination['total_rows'] = $this->dati_3_model->getDati3(NULL, NULL, $search_by, 'NUM_ROWS' );

                $this->pagination->initialize($config_pagination);

                $data['pagination']     = $this->pagination->create_links();
                $data['rows']      = $this->dati_3_model->getDati3($config_pagination['per_page'], $data['page_offset'], $search_by, 'PARTIAL' );
                $data['jml_segmen'] = $this->session->userdata('jml_segmen_article');
                $excel = $this->dati_3_model->getDati3($config_pagination['per_page'], $data['page_offset'], $search_by, 'EXCEL' );

                $this->session->set_userdata(__FUNCTION__.'_query', $excel);
                $this->session->set_userdata( 'sess_segment_'.__FUNCTION__, $param[3] );
                ( null !== $this->session->flashdata('result') ) ? $data['result'] = $this->session->flashdata('result') : $data['result'] = '';
                $data['search_by'] = $search_by;

                $content = $this->load->view('home/'.__FUNCTION__, $data, TRUE);

                $displayed = array(
                    'main_title' =>  $this->main_title,
                    'title' => 'Dati 3',
                    'content' => $content
                );

                $this->parser->parse('_template/master_home', $displayed);
                break;
        }//end switch
    }

    public function dati_4()
    {
        $data['func'] = __FUNCTION__;
        $param = $this->param;
        isset($param[3]) ? '' : $param[3] = '';
        $displayed = array();
        $data['sess_segment'] = $this->session->userdata('sess_segment');
        $data['base_path'] = strtolower(__CLASS__).'/'.__FUNCTION__;
        $post = $this->post;

        switch ($param[3]) {
            case 'export':
                $date_today = $today = date("Y_m_d_H_i_s");

                if ($param[4] == 'csv') {
                    $writer = WriterEntityFactory::createCSVWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_4_".$date_today.".csv");
                } else {
                    $writer = WriterEntityFactory::createXLSXWriter();
                    //stream to browser
                    $writer->openToBrowser("export_dati_4_".$date_today.".xlsx");
                }

                $header = [
                    WriterEntityFactory::createCell('ID'),
                    WriterEntityFactory::createCell('Nama'),
                    WriterEntityFactory::createCell('Kode Pos'),
                ];

                /** Tambah row satu kali untuk header */
                $singleRow = WriterEntityFactory::createRow($header);
                $writer->addRow($singleRow); //tambah row untuk header data

                $excel    = $this->session->userdata(__FUNCTION__.'_query');
                $SQL_get  = $excel;
                $RES_get  = $this->db->query($SQL_get);

                $i = 1;
                $data = array();

                foreach ( $RES_get->result_array() as $row ) {
                    $arr = [
                        WriterEntityFactory::createCell($row['id']),
                        WriterEntityFactory::createCell($row['nama']),
                        WriterEntityFactory::createCell($row['zip']),
                    ];

                    array_push($data, WriterEntityFactory::createRow($arr));
                }//end foreach

                $writer->addRows($data); // add multiple rows at a time
                $writer->close();
                break;

            case 'refresh':
                $this->session->unset_userdata(__FUNCTION__ . '_search_by');
                $this->session->unset_userdata(__FUNCTION__ . '_query');

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            case 'search_by':
                if (isset($post['id'])){ $search_by['id']      = $post['id']; }else{$search_by['id'] = ''; }
                if (isset($post['nama'])){ $search_by['nama']      = $post['nama']; }else{$search_by['nama'] = ''; }
                if (isset($post['kode_pos'])){ $search_by['kode_pos']      = $post['kode_pos']; }else{$search_by['kode_pos'] = ''; }

                $this->session->set_userdata( __FUNCTION__.'_search_by', $search_by );

                redirect(strtolower(__CLASS__).'/'.__FUNCTION__);
                break;

            default:
                $search_by = $this->session->userdata( __FUNCTION__.'_search_by');

                $this->table->set_template($this->tmpl);
                $this->table->set_heading('ID', 'Nama', 'Kode Pos');
                $data['page_offset']            = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                $config_pagination['per_page']  = '30';
                $config_pagination              = array_merge($this->config_pagination, $config_pagination);
                $config_pagination['base_url']  = site_url($data['base_path']);

                $data['total_rows']             = $config_pagination['total_rows'] = $this->dati_4_model->getDati4(NULL, NULL, $search_by, 'NUM_ROWS' );

                $this->pagination->initialize($config_pagination);

                $data['pagination']     = $this->pagination->create_links();
                $data['rows']      = $this->dati_4_model->getDati4($config_pagination['per_page'], $data['page_offset'], $search_by, 'PARTIAL' );
                $data['jml_segmen'] = $this->session->userdata('jml_segmen_article');
                $excel = $this->dati_4_model->getDati4($config_pagination['per_page'], $data['page_offset'], $search_by, 'EXCEL' );

                $this->session->set_userdata(__FUNCTION__.'_query', $excel);
                $this->session->set_userdata( 'sess_segment_'.__FUNCTION__, $param[3] );
                ( null !== $this->session->flashdata('result') ) ? $data['result'] = $this->session->flashdata('result') : $data['result'] = '';
                $data['search_by'] = $search_by;

                $content = $this->load->view('home/'.__FUNCTION__, $data, TRUE);

                $displayed = array(
                    'main_title' =>  $this->main_title,
                    'title' => 'Dati 4',
                    'content' => $content
                );

                $this->parser->parse('_template/master_home', $displayed);
                break;
        }//end switch
    }
}

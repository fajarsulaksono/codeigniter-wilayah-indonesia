<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('cleanInput'))
{
	function cleanInput($input) {

		$search = array(
		'@<script[^>]*?>.*?</script>@si',   // Strip out javascript
		'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
		'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
		'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		);

		$output = preg_replace($search, '', $input);
		return $output;
	}//end function
}

if (!function_exists('sanitize'))
{
	function sanitize($input) {
    	$output = '';
    	$ci =& get_instance();
		$ci->load->database();

		if (is_array($input)) {
			foreach($input as $var=>$val) {
				$output[$var] = sanitize($val);
			}
		}
		else {
			$input   = stripslashes($input);
			$input  = cleanInput($input);
			#$output = mysqli_real_escape_string($input);
			$output = $ci->db->escape_str($input);
		}
		return $output;
	}//end function
}

if (!function_exists('send_sms'))
{
    function send_sms($msisdn, $teks, $type)
    {
    	$ci =& get_instance();
		$ci->load->database();

		/*
			http://stackoverflow.com/questions/5724327/query-with-codeigniter-form-helper
			$sql = "YOUR SQL QUERY GOES HERE";
			$q = $ci->db->query($sql);
		*/
	
        $url 		= '';
		$user 		= '';
		$pass 		= '';
		$sender_id 	= '';

        $url = $url."?user=".$user."&pwd=".$pass."&sender=".$sender_id."&msisdn=".$msisdn."&message=".$teks;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		
		#var_dump($output);

		$xml = simplexml_load_string($output);
		//print_r($xml);
		//echo 'TrxID : '.$xml->TrxID;
		$trxid = $xml->TrxID;
		$status = $xml->Status;

		if(!isset($type)){
			$type = '';
		}

		$sql_log = "INSERT INTO sms_log (url, trx_id, status, type) VALUES ('$url', '$trxid', '$status', '$type')";		
		$ci->db->query($sql_log);

        if ($status === 'SENT'){
			return TRUE;
        }else{
        	return FALSE;
        }
        
    }//
}//if

if (!function_exists('rupiah'))
{
	function rupiah($nilai, $pecahan = 0) {
		return number_format($nilai, $pecahan, ',', '.');
	}
}//if


/**
 * Dump helper. Functions to dump variables to the screen, in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) 
{
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        
        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
        
        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}


if (!function_exists('dump_exit')) 
{
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');

		$this->load->helper(array('utility'));

		if (!$this->ion_auth->logged_in()){
			//redirect them to the login page
			//redirect('auth/login', 'refresh');
		}

		// sanitize all input _POST
		// $_POST = sanitize($_POST);

		// global var :
		$this->date_today  = date("Y_m_d_H_i_s");

		/*
		http://stackoverflow.com/questions/283004/getting-the-name-of-a-child-class-in-the-parent-class-static-context
		 */
		$this->class_name  = strtolower(get_class($this));

		$this->tmpl = array (
		  'table_open'   => '<div class="table-responsive">
						<table  width="100%"
							class="table table-bordered table-responsive table-striped table-hover"
							border="0"
							cellpadding="4"
							cellspacing="0">',
		  'heading_row_start'   => '<tr>',
		  'heading_row_end'     => '</tr>',
		  'heading_cell_start'  => '<th>',
		  'heading_cell_end'    => '</th>',
		  'row_start'           => '<tr>',
		  'row_end'             => '</tr>',
		  'cell_start'          => '<td>',
		  'cell_end'            => '</td>',
		  'row_alt_start'       => '<tr>',
		  'row_alt_end'         => '</tr>',
		  'cell_alt_start'      => '<td>',
		  'cell_alt_end'        => '</td>',
		  'table_close'         => '</table></div>'
		  );

		$this->config_pagination['first_link']    = 'First';
		$this->config_pagination['last_link']     = 'Last';
		$this->config_pagination['prev_link']    = 'Previous';
		$this->config_pagination['next_link']     = 'Next';
		$this->config_pagination['attributes'] 		= array('class' => 'page-link');
		$this->config_pagination['full_tag_open']   = '<ul class="pagination">';
		$this->config_pagination['full_tag_close']  = "</ul>";
		$this->config_pagination['cur_tag_open']  = '<li class="page-item active"><a class="page-link" href="javascript:void(0)">';
		$this->config_pagination['cur_tag_close']   = "</a></li>";
		$this->config_pagination['first_tag_open']  = '<li class="page-item">';
		$this->config_pagination['first_tag_close']   = "</li>";
		$this->config_pagination['prev_tag_open']  = '<li class="page-item">';
		$this->config_pagination['prev_tag_close']   = "</li>";
		$this->config_pagination['num_tag_open']  = '<li class="page-item">';
		$this->config_pagination['num_tag_close']   = "</li>";
		$this->config_pagination['next_tag_open']  = '<li class="page-item">';
		$this->config_pagination['next_tag_close']   = "</li>";
		$this->config_pagination['last_tag_open']  = '<li class="page-item">';
		$this->config_pagination['last_tag_close']   = "</li>";

		$this->user_id = $this->session->userdata('user_id');

	}//end __construct()

	public function get_uri_array()
	{
		$param = array();
		for ($i=1; $i < 10 ; $i++) {
			if ($this->uri->segment($i) != false) {
				$param[$i] = $this->uri->segment($i);
				#var_dump($param[$i]);
			}
		}

		return $param;
	}// end get_uri_array()

	public function get_language()
	{
		return (null !== $this->session->userdata('language')) ? $this->session->userdata('language') : 'id';
	}

	public function reset_lang()
	{
		  $this->session->unset_userdata('current_page');

		  if ($this->get_language()){
			$data['language'] = $this->get_language();
		  }else{
			$data['language'] = 'id';
		  }

		  return $data;
	}//end reset_lang

	public function _render_page($view, $template, $data = [])
	{
		$this->viewdata = (empty($data)) ? $this->data : $data;
		$content        = $this->load->view($view, $this->viewdata, TRUE);
		$data['lang'] 	= ( null !== $this->session->userdata('language') ) ? $this->session->userdata('language') : 'en' ;
		$header         = $this->load->view('_template/master_header', $data, TRUE);
		$footer         = $this->load->view('_template/master_footer', $data, TRUE);

		$data = array(
			'title'     => $data['title'],
			'content'   => $content,
			'header'    => $header,
			'footer'    => $footer
		);

		$this->parser->parse('_template/'.$template, $data);
	}//end _render_page

}// end MY_Controller class

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
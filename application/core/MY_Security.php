<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Security extends CI_Security {

    public function __construct()
    {
        parent::__construct();
    }

    /**
    http://stackoverflow.com/questions/14406922/prevent-from-displaying-the-default-csrf-error-page-in-codeigniter
    */
    public function csrf_show_error()
    {
        header('Location:'.htmlspecialchars($_SERVER['REQUEST_URI']));
    }//end function

}//end class